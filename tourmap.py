import facebook
graph = facebook.GraphAPI(access_token="EAADu5LUUqt4BADjF5VahfkWr89Lrf79o0No3zlNUyAVOPP1IHqav2ZB8WOGGP5hZBv2hAm13MKX50mGKy1jiHrX2E0RT4ZBrydzaCPHZAbkHnfVTE4jYbYoQNpPIHnpeb27QSZAuQvvadZBynLqZBKnFoIK4sf12In5x6T0ZC9Ux21wo3ZAJkeyTpc2yEgTMF4FiaZCLLvQRXerQZDZD", version="3.1")

ids = [1685613338122475]

csv = ""

def getPageEvents(page_id):
    global csv
    print("Querying events for page id %d" % page_id)
    page = graph.get_object(id=page_id, fields="name, events")
    events = page['events']['data']

    for event in events:
        print(event)
        csv += page['name'] + ','
        csv += event['name'] + ','
        csv += event['place']['name'] + ','
        csv += event['place']['location']['city'] + ','
        csv += event['place']['location']['state'] + ','
        csv += event['start_time'] + ','
        csv += (event['attending_count'] if ('attending_count' in event) else '0') + ','
        csv += (event['interested_count'] if ('interested_count' in event) else '0') + ','
        csv += "\"" + event['description'] + "\","
        csv += event['place']['id'] + ','
        csv += event['id'] + '\n'

for id in ids:
    getPageEvents(id)

with open('tourmap.csv', 'w') as file:
    file.write(csv)
